var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
// include docentes controller
var docentesCtrl = require(path.join(ctrlDir, "docentes"));
var alumnosCtrl = require(path.join(ctrlDir, "alumnos"));

// routes chat
router.get("/", function (req, res, next) {
    res.render("chat", { title: "Express" });
});

router.get("/chat", function (req, res, next) {
    res.render("chat", { title: "Express" });
});

// route asignatura under construction

router.get("/new", async function (req, res, next) {
    var myData = await docentesCtrl.load();
    var alumnos = await alumnosCtrl.load();
    res.render("newAsignatura", {'myData': myData, 'alumnos': alumnos});
});




// route asignatu post(get for now) => altaAsignatura
router.get("/altaAsignatura", function (req, res, next) {
    console.log(req.query);
    res.redirect("/new");
});

// page not found route
router.get("*", function (req, res, next) {
    var error;
    if(res.error) error = res.error;
    res.render("error", error);
});

module.exports = router;
