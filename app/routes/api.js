var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");

//Controladores
var asignaturaCtrl = require(path.join(ctrlDir, "asignaturas"));

// Asignatura save to asignatura/api/get form
router.route('/asignatura/new').get((req, res, next) => {
    var newAsignatura = req.query;
    asignaturaCtrl.save(newAsignatura);
    res.send("Guardado");
})

module.exports = router;
