var mongoose = require('mongoose');
Schema = mongoose.Schema;

var asignaturaSchema = new mongoose.Schema({
    nombre: { type: String },
    num_horas: { type: String },
    docente: Schema.Types.ObjectId,
    alumnos: [Schema.Types.ObjectId]
});

module.exports = mongoose.model("Asignatura", asignaturaSchema);


