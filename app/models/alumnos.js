var mongoose = require('mongoose');
Schema = mongoose.Schema;

var alumnoSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    nombre: { type: String },
    apellido: { type: String }
});

module.exports = mongoose.model("Alumno", alumnoSchema);