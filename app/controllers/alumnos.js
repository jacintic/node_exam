var mongoose = require("mongoose"),
    Alumno = require("../models/alumnos");

// load from DB function
exports.load =  async(req, res, next) => {
    var result =  await Alumno.find({});
    return result;
}