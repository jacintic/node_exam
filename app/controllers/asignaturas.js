var mongoose = require("mongoose"),
    Asignatura = require("../models/asignatura");

// save to DB function
exports.save = async (req, res, next) => {
    try{
        var newAsignatura = new Asignatura(req);
        var res = await newAsignatura.save();
        console.log("Insertado asignatura en la BD");
        return res;
    }catch(e){
        console.log(e);
    }
}
