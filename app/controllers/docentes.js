var mongoose = require("mongoose"),
    Docente = require("../models/docente");

// load from DB function
exports.load =  async(req, res, next) => {
    var result =  await Docente.find({});
    return result;
}