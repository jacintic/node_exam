require("dotenv").config();
const port = process.env.SERVER_PORT || 6000;
var express = require("express"),
  app = express(),
  server = require("http").createServer(app),
  path = require("path"),
  mongoose = require('mongoose'),
  io = require('socket.io')(server);

server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});


//Connect to mongodb://devroot:devroot@mongo:27017/chat?authSource=admin 
mongoose.connect(     
  `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,     
  { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },     
  (err, res) => {     
    if (err) console.log(`ERROR: connecting to Database. ${err}`);     
    else console.log(`Database Online: ${process.env.MONGO_DB}`);     
  });     
  console.log(`mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`);


// Import routes of our app

var routes = require("./app/routes/app");
var handlerError = require("./app/routes/handler");

// view engine setup and other configurations
app.set("views", path.join(__dirname,"app","views"));
app.set("view engine", "pug");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

// Define api routes
var api = require("./app/routes/api");

// //API
app.use('/api',(req, res, next) =>{
  req.isAPI=true;
  next();
});
app.use('/api',api);

// Define routes using URL path

app.use("/", routes);
app.use(handlerError);

/*Socket functions */
var socketsRouter = require("./app/routes/sockets");
// Define routes using URL path
app.use("/", socketsRouter);

// more sockets
//user tracking
var numUsers = 0;

// testing message
var messages = [
  {numUsers: numUsers
  },{
    author: "Carlos",
    text: "Hola! que tal?"
  },{
    author: "Pepe",
    text: "Muy bien! y tu??"  
  },{
    author: "Paco",
    text: "Genial!"
}];


/*Socket functions */
io.on('connection', function(socket) {
  console.log('Un cliente se ha conectado',messages[0].numUsers += 1 );
  // send messages once client is connected
  socket.emit('messages', messages);

  //listen to new messages from chat
  socket.on('newMessage', (data) => {
    // monitoring new messages
    console.log("new message from app: "+ data.user + ": " + data.msg);
    // send message to the rest of the clients
    socket.broadcast.emit('newMessage', data)

  
  });
  
  

});

module.exports = app; 
