$(document).ready(function () {
  //Inicializa socket con IO

  //Cuando cambia el select redirigimos a la URL del chat
  /*
  $('#selectRoom').on("change",()=>{
    var sala = $(this).find("option:selected").val();
    window.location.href = "/chat/"+sala;
  })
  */
 $('#selectRoom').on("change",()=>{
  var sala = $(this).find("option:selected").val();

})
  //Accion cuando el usuario envia mensaje con submit
  $("#chat").submit(function (e) {
    e.preventDefault();
    var usr = $("#autor").val();
    var msg = $("#msg").val();
    $("#chatBox").append(`<p><span>${usr}:</span> ${msg}<p>`);
    var message = {
      user: `${usr}`,
      msg: `${msg}`   // msg
    }
    var myDiv = $("#chatBox");
    $("#chatBox").scrollTop(myDiv.prop("scrollHeight"));
    socket.emit('newMessage', message);
  });

  
});
//Acciones a realizar cuando se detecta actividad en el canal newMsg
const recieveMessage = (message) => {
  console.log(message);
  // append text to chat
  $("#chatBox").append("<p><span>"+ message.user +  ":</span>" + message.msg + "</p>");
  var myDiv = $("#chatBox");
  $("#chatBox").scrollTop(myDiv.prop("scrollHeight"));
}
